1. How to run this app?

You should get java 7 installed on your machine.

1) Import it to your IDE (e.g. Intellij Or Eclipse) and run App in it.

2) Run it using maven.
  2.1) Go to project Home and Run: mvn compile package.
  2.2) There will be two jar files created in target folder, one jar file does not contains dependencies and another does.
  2.3) Run: java -jar target/CovataMarsRover-1.0-jar-with-dependencies.jar inputFilePath


2. Design:

1) App.java is the entry of this application.
2) MarsExplorer class is the main application.
  2.1) It consists of:
     2.1.1) a Mars plateau to explore.
     2.1.2) a list of Rovers and a exploration instruction for each Rover.
  2.2) When a MarsExplorer is created, you can use a input file to initialize it.
  2.3) When a MarsExplorer is initialized, you can call explore() method to perform Mars Exploration.
3) Rover class define what is a Rover and what can a Rover do.
  3.1) A Rover has a name, a initial position and a plateau to explore.
  3.2) A Rover can turn left OR right.
  3.3) A Rover can forward towards its heading direction (defined in position object).
  3.4) A Rover can move by following the given instructions (a combination of turn left, turn right and forward).
4) Position class:
 A position is represented by a combination of x and y co-ordinates and a letter representing one of the four cardinal
 compass points. E.g: 0, 0, N.


3. Assumptions

1) Rovers could not move out of a given plateau.
2) The given input file always:
  2.1) first line is: the plateau upperRight coordinate.
  2.2) then following lines:
       2.2.1) A line is Rover definition
       2.2.2) A line is Rover exploration instructions.
       2.2.3) repeat 2.2.1 and 2.2.2.
3) If a empty input file provided, then do nothing.
4) If only plateau definition provided in input file, then do nothing.
5) For a Rover, if the exploration instruction is empty, then Rover will not move.