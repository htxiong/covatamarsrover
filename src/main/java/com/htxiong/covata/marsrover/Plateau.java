package com.htxiong.covata.marsrover;

public class Plateau {
    private Coordinate upperRight;
    private Coordinate bottomLeft;

    public Plateau() {
        this.upperRight = new Coordinate(0, 0);
        this.bottomLeft = new Coordinate(0, 0);
    }

    public Plateau(Coordinate upperRight) {
        this();
        this.upperRight = upperRight;
    }

    public Coordinate getUpperRight() {
        return upperRight;
    }

    public void setUpperRight(Coordinate upperRight) {
        this.upperRight = upperRight;
    }

    public Coordinate getBottomLeft() {
        return bottomLeft;
    }

    public void setBottomLeft(Coordinate bottomLeft) {
        this.bottomLeft = bottomLeft;
    }

    @Override
    public String toString() {
        return "bottomLeft is " + this.bottomLeft + ", and upperRight is " + this.upperRight;
    }

    public boolean isInPlateau(Coordinate coordinate) {
        return coordinate.getX() >= this.bottomLeft.getX() && coordinate.getY() >= this.bottomLeft.getY()
                && coordinate.getX() <= this.upperRight.getX() && coordinate.getY() <= this.getUpperRight().getY();
    }
}
