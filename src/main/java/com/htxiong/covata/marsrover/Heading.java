package com.htxiong.covata.marsrover;

import org.apache.commons.lang3.StringUtils;

public enum Heading {
    N("N"), S("S"), E("E"), W("W");
    String name;
    int value;

    Heading(String name) {
        this.name = name;
        switch (this.name) {
            case "N":
                value = 1;
                break;
            case "E":
                value = 2;
                break;
            case "S":
                value = 3;
                break;
            case "W":
                value = 4;
                break;
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public static Heading fromCode(String code) {
        Heading result;

        if (StringUtils.isBlank(code)) {
            throw new IllegalArgumentException("Empty string is not a valid Heading");
        } else {
            try {
                result = valueOf(code.toUpperCase());
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException("The given Heading is invalid: " + code);
            }
        }

        return result;
    }

    public static Heading fromValue(int value) {
        Heading result = N;
        int mod = value % 4;
        switch (mod) {
            case 1:
                result = N;
                break;
            case 2:
                result = E;
                break;
            case 3:
                result = S;
                break;
            case 0:
                result = W;
                break;
        }

        return result;
    }
}
