package com.htxiong.covata.marsrover;

import org.apache.commons.lang3.StringUtils;

public class Rover {
    private String name;
    private Position position;
    private Plateau plateau;

    public Rover(String name, Position position, Plateau plateau) {
        if (plateau.isInPlateau(position.getCoordinate())) {
            this.name = name;
            this.position = position;
            this.plateau = plateau;
        } else {
            throw new IllegalArgumentException("The init coordinate [" + position.getCoordinate() + "] of Rover " + name +
                    " is out of the given plateau: " + plateau);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public void move(String instructions) {
        if (StringUtils.isNotBlank(instructions)) {
            instructions = StringUtils.strip(instructions.toUpperCase());
            String regex = "[L|M|R]+";
            if (instructions.matches(regex)) {
                for (int i = 0; i < instructions.length(); i++) {
                    String instruction = String.valueOf(instructions.charAt(i));
                    if ("L".equalsIgnoreCase(instruction)) {
                        turnLeft();
                    } else if ("R".equalsIgnoreCase(instruction)) {
                        turnRight();
                    } else if ("M".equalsIgnoreCase(instruction)) {
                        forward();
                    }
                }
            } else {
                throw new IllegalArgumentException("Rover explore instruction [" + instructions + "] is not valid.");
            }
        }
    }

    public void turnLeft() {
        Heading currentHeading = this.getPosition().getHeading();
        Heading newHeading = Heading.fromValue(currentHeading.getValue() - 1);
        this.getPosition().setHeading(newHeading);
    }

    public void turnRight() {
        Heading currentHeading = this.getPosition().getHeading();
        Heading newHeading = Heading.fromValue(currentHeading.getValue() + 1);
        this.getPosition().setHeading(newHeading);
    }

    public void forward() {
        switch (this.position.getHeading()) {
            case N:
                forwardToNorth();
                break;
            case S:
                forwardToSouth();
                break;
            case E:
                forwardToEast();
                break;
            case W:
                forwardToWest();
                break;
            default:
                break;
        }
    }

    private void forwardToNorth() {
        Coordinate newCoordinate = new Coordinate(this.getPosition().getCoordinate().getX(), this.getPosition().getCoordinate().getY() + 1);
        validateMove(newCoordinate);

        this.getPosition().setCoordinate(newCoordinate);
    }

    private void forwardToSouth() {
        Coordinate newCoordinate = new Coordinate(this.getPosition().getCoordinate().getX(), this.getPosition().getCoordinate().getY() - 1);
        validateMove(newCoordinate);

        this.getPosition().setCoordinate(newCoordinate);
    }

    private void forwardToEast() {
        Coordinate newCoordinate = new Coordinate(this.getPosition().getCoordinate().getX() + 1, this.getPosition().getCoordinate().getY());
        validateMove(newCoordinate);

        this.getPosition().setCoordinate(newCoordinate);
    }

    private void forwardToWest() {
        Coordinate newCoordinate = new Coordinate(this.getPosition().getCoordinate().getX() - 1, this.getPosition().getCoordinate().getY());
        validateMove(newCoordinate);

        this.getPosition().setCoordinate(newCoordinate);
    }

    private void validateMove(Coordinate newCoordinate) {
        if (!this.plateau.isInPlateau(newCoordinate)) {
            throw new IllegalArgumentException("The coordinate [" + newCoordinate + "] where " + this.name + " is moving to is out of plateau: " + this.plateau);
        }
    }
}
