package com.htxiong.covata.marsrover;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class MarsExplorer {
    private Plateau plateau;
    private Set<Rover> rovers;
    private Map<String, String> roverInstructions;

    public MarsExplorer() {
        this.plateau = new Plateau();
        this.rovers = new LinkedHashSet<>();
        this.roverInstructions = new HashMap<>();
    }

    /**
     * initialize a MarsExplorer by providing a input file which contains the definition of Mars' Plateau,
     * a list of Rovers and a exploration instructions for each of these Rover.
     *
     * @param filePath the path of input file.
     */
    public void initialize(String filePath) {
        if (StringUtils.isBlank(filePath)) {
            throw new IllegalArgumentException("Please provide input file for initialization.");
        }

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filePath));

            int index = 0;
            Rover newRover = null;
            String item;
            while ((item = bufferedReader.readLine()) != null) {
                if (index == 0) {
                    initializePlateau(item);
                    index++;
                } else {
                    if (newRover == null) {
                        String name = "Rover #" + index;
                        newRover = createRover(name, item);
                        index++;
                    } else {
                        roverInstructions.put(newRover.getName(), item);
                        newRover = null;
                    }
                }
            }

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("The input file you provided is not found.");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                if (bufferedReader != null) bufferedReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void initializePlateau(String plateauDefinition) {
        plateauDefinition = StringUtils.strip(plateauDefinition);
        if (StringUtils.isNotBlank(plateauDefinition)) {
            String[] plateauParams = StringUtils.split(plateauDefinition);
            if (plateauParams.length == 2) {
                try {
                    int x = Integer.parseInt(plateauParams[0].trim());
                    int y = Integer.parseInt(plateauParams[1].trim());

                    Coordinate coordinate = new Coordinate(x, y);
                    this.plateau.setUpperRight(coordinate);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("There is a error found when initialize plateau: " + plateauDefinition + ". And " +
                            "the error message is: " + e.getMessage());
                }
            } else {
                throw new IllegalArgumentException("The give input for plateau is incorrect: " + plateauDefinition);
            }
        } else {
            throw new IllegalArgumentException("Please provide a valid input for plateau.");
        }
    }

    private Rover createRover(String name, String roverDefinition) {
        Rover rover;

        roverDefinition = StringUtils.strip(roverDefinition);
        if (StringUtils.isNotBlank(roverDefinition)) {
            String[] plateauParams = StringUtils.split(roverDefinition);
            if (plateauParams.length == 3) {
                try {
                    int x = Integer.parseInt(plateauParams[0].trim());
                    int y = Integer.parseInt(plateauParams[1].trim());
                    Coordinate coordinate = new Coordinate(x, y);

                    String heading = plateauParams[2];
                    Position position = new Position(coordinate, Heading.fromCode(heading));

                    rover = new Rover(name, position, this.plateau);
                    this.rovers.add(rover);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("There is a error found when initialize an new rover: " + roverDefinition + ". And " +
                            "the error message is: " + e.getMessage());
                }
            } else {
                throw new IllegalArgumentException("The give input for create Rover is incorrect: " + roverDefinition);
            }
        } else {
            throw new IllegalArgumentException("Please provide a valid input for creating an Rover.");
        }

        return rover;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public Set<Rover> getRovers() {
        return rovers;
    }

    public void explore() {
        for (Rover rover : this.rovers) {
            String instruction = this.roverInstructions.get(rover.getName());
            rover.move(instruction);
            System.out.println(rover.getPosition());
        }
    }
}
