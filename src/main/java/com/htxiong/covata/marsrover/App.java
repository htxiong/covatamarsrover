package com.htxiong.covata.marsrover;

public class App 
{
    public static void main( String[] args )
    {
        if(args.length == 1) {
            String inputFilePath = args[0];

            MarsExplorer marsExplorer = new MarsExplorer();
            marsExplorer.initialize(inputFilePath);
            marsExplorer.explore();
        } else if(args.length == 0) {
            throw new IllegalArgumentException("Please provide 1 argument, its the path of the input file.");
        } else if(args.length > 1) {
            throw new IllegalArgumentException("Please provide only 1 argument, its the path of the input file.");
        }
    }
}
