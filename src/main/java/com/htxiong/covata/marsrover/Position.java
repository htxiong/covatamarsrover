package com.htxiong.covata.marsrover;

public class Position {
    private Coordinate coordinate;
    private Heading heading;

    public Position(Coordinate coordinate, Heading heading) {
        this.coordinate = coordinate;
        this.heading = heading;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Heading getHeading() {
        return heading;
    }

    public void setHeading(Heading heading) {
        this.heading = heading;
    }

    @Override
    public String toString() {
        return coordinate + " " + heading;
    }
}
