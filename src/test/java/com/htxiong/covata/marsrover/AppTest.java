package com.htxiong.covata.marsrover;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AppTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testAppWithOneArgument() throws Exception {
        String path = "/test_file_valid.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path resourcePath = Paths.get(resourceUrl.toURI());
        String[] args = {resourcePath.toString()};
        App.main(args);
    }

    @Test
    public void testAppWithMoreThanOneArguments() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Please provide only 1 argument, its the path of the input file.");

        String path = "/test_file_valid.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path resourcePath = Paths.get(resourceUrl.toURI());
        String[] args = {resourcePath.toString(), "2", "3"};
        App.main(args);
    }

    @Test
    public void testAppWithZeroArguments() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Please provide 1 argument, its the path of the input file.");

        String[] args = {};
        App.main(args);
    }

    @Test
    public void testAppWithOneArguments_NotExistingFilePath() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The input file you provided is not found.");

        String path = "/test_not_existing.txt";
        assert getClass().getResource(path) == null;

        String[] args = {path};
        App.main(args);
    }
}
