package com.htxiong.covata.marsrover;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PositionTest {
    private Coordinate coordinate = null;
    private Position position = null;

    @Before
    public void setUp() throws Exception {
        coordinate = new Coordinate(1, 2);
        position = new Position(coordinate, Heading.N);
    }

    @After
    public void tearDown() throws Exception {
        coordinate = null;
        position = null;
    }

    @Test
    public void testToString() throws Exception {
        assert "1 2 N".equals(position.toString());

        // given
        position.setHeading(Heading.S);

        // Then
        assert "1 2 S".equals(position.toString());

        // Given
        position.getCoordinate().setX(4);
        position.getCoordinate().setY(7);
        assert "4 7 S".equals(position.toString());
    }
}