package com.htxiong.covata.marsrover;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CoordinateTest {

    public Coordinate coordinate = null;

    @Before
    public void setUp() throws Exception {
        coordinate = new Coordinate(1, 2);
    }

    @After
    public void tearDown() throws Exception {
        coordinate = null;
    }

    @Test
    public void testToString() throws Exception {
        assert "1 2".equals(coordinate.toString());
    }
}