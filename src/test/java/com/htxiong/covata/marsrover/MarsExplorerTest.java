package com.htxiong.covata.marsrover;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MarsExplorerTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private MarsExplorer marsExplorer = null;

    @Before
    public void setUp() throws Exception {
        marsExplorer = new MarsExplorer();
    }

    @After
    public void tearDown() throws Exception {
        marsExplorer = null;
    }

    @Test
    public void testInitialize_WithNoInputFile() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Please provide input file for initialization.");

        // Given
        String filePath = null;

        // When
        marsExplorer.initialize(filePath);
    }

    @Test
    public void testInitialize_WithNoInputFile2() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Please provide input file for initialization.");

        // Given
        String filePath = "";

        // When
        marsExplorer.initialize(filePath);
    }

    @Test
    public void testInitialize_WithNotExistingInputFile() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The input file you provided is not found.");

        // Given
        String filePath = "/test0.txt";

        // When
        marsExplorer.initialize(filePath);
    }

    @Test
    public void testInitialize_WithEmptyInputFile() throws Exception {
        // Given
        String path = "/test_file_empty.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());

        // Then
        assert marsExplorer.getPlateau().getBottomLeft().getX() == 0;
        assert marsExplorer.getPlateau().getBottomLeft().getY() == 0;
        assert marsExplorer.getPlateau().getUpperRight().getX() == 0;
        assert marsExplorer.getPlateau().getUpperRight().getY() == 0;

        assert marsExplorer.getRovers().isEmpty();
    }

    @Test
    public void testInitialize_WithWorkingInputFile() throws Exception {
        // Given
        String path = "/test_file_valid.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());

        // Then
        assert marsExplorer.getRovers().size() == 2;
        assert marsExplorer.getPlateau().getBottomLeft().getX() == 0;
        assert marsExplorer.getPlateau().getBottomLeft().getY() == 0;
        assert marsExplorer.getPlateau().getUpperRight().getX() == 5;
        assert marsExplorer.getPlateau().getUpperRight().getY() == 5;

        int index = 1;
        for (Rover rover : marsExplorer.getRovers()) {
            if (index == 1) {
                assert "Rover #1".equals(rover.getName());
                assert rover.getPosition().getCoordinate().getX() == 1;
                assert rover.getPosition().getCoordinate().getY() == 2;
                assert rover.getPosition().getHeading() == Heading.N;
            } else {
                assert "Rover #2".equals(rover.getName());
                assert rover.getPosition().getCoordinate().getX() == 3;
                assert rover.getPosition().getCoordinate().getY() == 3;
                assert rover.getPosition().getHeading() == Heading.E;
            }

            index ++;
        }
    }

    @Test
    public void testExploreMars_WithValidInputs() throws Exception {
        // Given
        String path = "/test_file_valid.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());
        marsExplorer.explore();

        // Then
        assert marsExplorer.getRovers().size() == 2;
        assert marsExplorer.getPlateau().getBottomLeft().getX() == 0;
        assert marsExplorer.getPlateau().getBottomLeft().getY() == 0;
        assert marsExplorer.getPlateau().getUpperRight().getX() == 5;
        assert marsExplorer.getPlateau().getUpperRight().getY() == 5;

        int index = 1;
        for (Rover rover : marsExplorer.getRovers()) {
            if (index == 1) {
                assert "Rover #1".equals(rover.getName());
                assert rover.getPosition().getCoordinate().getX() == 1;
                assert rover.getPosition().getCoordinate().getY() == 3;
                assert rover.getPosition().getHeading() == Heading.N;
            } else {
                assert "Rover #2".equals(rover.getName());
                assert rover.getPosition().getCoordinate().getX() == 5;
                assert rover.getPosition().getCoordinate().getY() == 1;
                assert rover.getPosition().getHeading() == Heading.E;
            }

            index ++;
        }
    }

    @Test
    public void testExploreMars_WithValidInputsAndNoInstructions() throws Exception {
        // Given
        String path = "/test_file_valid_without_instruction.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());
        marsExplorer.explore();

        // Then
        assert marsExplorer.getRovers().size() == 1;
        assert marsExplorer.getPlateau().getBottomLeft().getX() == 0;
        assert marsExplorer.getPlateau().getBottomLeft().getY() == 0;
        assert marsExplorer.getPlateau().getUpperRight().getX() == 5;
        assert marsExplorer.getPlateau().getUpperRight().getY() == 5;

        for (Rover rover : marsExplorer.getRovers()) {
            assert "Rover #1".equals(rover.getName());
            assert rover.getPosition().getCoordinate().getX() == 1;
            assert rover.getPosition().getCoordinate().getY() == 2;
            assert rover.getPosition().getHeading() == Heading.N;
        }
    }

    @Test
    public void testExploreMars_WithInvalidInstructions() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("The coordinate [5 3] where Rover #2 is moving to is out of plateau: bottomLeft is 0 0, and upperRight is 4 4");

        // Given
        String path = "/test_file_invalid_instructions.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());
        marsExplorer.explore();
    }

    @Test
    public void testExploreMars_WithInvalidRoverPosition() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("The init coordinate [6 2] of Rover Rover #1 is out of the given plateau: bottomLeft is 0 0, and upperRight is 5 5");

        // Given
        String path = "/test_file_invalid_position.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());
        marsExplorer.explore();
    }

    @Test
    public void testExploreMars_WithInvalidRoverDefinition() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("The give input for create Rover is incorrect: 6 2");

        // Given
        String path = "/test_file_invalid_rover_definition.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());
        marsExplorer.explore();
    }

    @Test
    public void testExploreMars_WithInvalidHeading() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("The given Heading is invalid: K");

        // Given
        String path = "/test_file_invalid_rover_heading.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());
        marsExplorer.explore();
    }

    @Test
    public void testExploreMars_WithInvalidPlateauDefinition() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("The give input for plateau is incorrect: ");

        // Given
        String path = "/test_file_invalid_plateau_definition.txt";
        assert getClass().getResource(path) != null;
        URL resourceUrl = getClass().getResource(path);
        Path filePath = Paths.get(resourceUrl.toURI());

        // When
        marsExplorer.initialize(filePath.toString());
        marsExplorer.explore();
    }
}