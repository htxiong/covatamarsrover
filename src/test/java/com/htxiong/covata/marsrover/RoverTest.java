package com.htxiong.covata.marsrover;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RoverTest {
    @Rule
    public ExpectedException thrown= ExpectedException.none();
    private Rover rover = null;
    private Position position = null;
    private Plateau plateau = null;

    @Before
    public void setUp() throws Exception {
        position = new Position(new Coordinate(1, 2), Heading.N);
        plateau = new Plateau(new Coordinate(5, 5));
        rover = new Rover("Rover #1", position, plateau);
    }

    @After
    public void tearDown() throws Exception {
        position = null;
        plateau = null;
        rover = null;
    }

    @Test
    public void testMove_InPlateau1() throws Exception {
        // Given
        String instructions = "LMLMLMLMM";

        // When
        rover.move(instructions);

        // Then
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 3;
        assert rover.getPosition().getHeading() == Heading.N;
    }

    @Test
    public void testMove_InPlateau2() throws Exception {
        // Given
        String instructions = "MMRMMRMRRM";
        Position position2 = new Position(new Coordinate(3, 3), Heading.E);
        rover.setPosition(position2);

        // When
        rover.move(instructions);

        // Then
        assert rover.getPosition().getCoordinate().getX() == 5;
        assert rover.getPosition().getCoordinate().getY() == 1;
        assert rover.getPosition().getHeading() == Heading.E;
    }

    @Test
    public void testMove_OutPlateau() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The coordinate [-1 3] where " + rover.getName() + " is moving to is out of plateau: " + plateau);

        // Given
        String instructions = "LMLMLMLMMLMM";

        // When
        rover.move(instructions);
    }

    @Test
    public void testMove_WithTurnLeftRightOnlyInstructions() throws Exception {
        // Given
        String instructions = "LLLLLLLL";

        // When
        rover.move(instructions);

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // Given
        instructions = "RRRRRRRR";

        // When
        rover.move(instructions);

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;
    }

    @Test
    public void testMove_WithEmptyInstructions() throws Exception {
        // Given
        String instructions = "";

        // When
        rover.move(instructions);

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;
    }

    @Test
    public void testMove_WithNullAsInstructions() throws Exception {
        // Given
        String instructions = null;

        // When
        rover.move(instructions);

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;
    }

    @Test
    public void testMove_WithSpacesAsInstructions() throws Exception {
        // Given
        String instructions = "            ";

        // When
        rover.move(instructions);

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;
    }

    @Test
    public void testMove_WithInvalidInstructions1() throws Exception {
        // Given
        String instructions = "LMMTT";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Rover explore instruction [" + instructions + "] is not valid.");

        // When
        rover.move(instructions);
    }

    @Test
    public void testMove_WithInvalidInstructions2() throws Exception {
        // Given
        String instructions = "11231";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Rover explore instruction [" + instructions + "] is not valid.");

        // When
        rover.move(instructions);
    }

    @Test
    public void testTurnLeft() throws Exception {
        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.W;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.S;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;
    }

    @Test
    public void testTurnRight() throws Exception {
        // When
        rover.turnRight();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.turnRight();

        // Then
        assert rover.getPosition().getHeading() == Heading.S;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.turnRight();

        // Then
        assert rover.getPosition().getHeading() == Heading.W;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.turnRight();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;
    }

    @Test
    public void testForwardInPlateau() throws Exception {
        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.W;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.W;
        assert rover.getPosition().getCoordinate().getX() == 0;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.S;
        assert rover.getPosition().getCoordinate().getX() == 0;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.S;
        assert rover.getPosition().getCoordinate().getX() == 0;
        assert rover.getPosition().getCoordinate().getY() == 1;

        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 0;
        assert rover.getPosition().getCoordinate().getY() == 1;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 1;

        // When
        rover.turnLeft();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 1;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 3;
    }

    @Test
    public void testForwardOutPlateauTowardsToNorth() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The coordinate [1 6] where " + rover.getName() + " is moving to is out of plateau: " + plateau);

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 3;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 4;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.N;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 5;

        // When
        rover.forward();
    }

    @Test
    public void testForwardOutPlateauTowardsToSouth() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The coordinate [1 -1] where " + rover.getName() + " is moving to is out of plateau: " + plateau);

        // When
        rover.getPosition().setHeading(Heading.S);
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.S;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 1;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.S;
        assert rover.getPosition().getCoordinate().getX() == 1;
        assert rover.getPosition().getCoordinate().getY() == 0;

        // When
        rover.forward();
    }

    @Test
    public void testForwardOutPlateauTowardsToWest() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The coordinate [-1 2] where " + rover.getName() + " is moving to is out of plateau: " + plateau);

        // When
        rover.getPosition().setHeading(Heading.W);
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.W;
        assert rover.getPosition().getCoordinate().getX() == 0;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();
    }

    @Test
    public void testForwardOutPlateauTowardsToEast() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The coordinate [6 2] where " + rover.getName() + " is moving to is out of plateau: " + plateau);

        // When
        rover.getPosition().setHeading(Heading.E);
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 2;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 3;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 4;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();

        // Then
        assert rover.getPosition().getHeading() == Heading.E;
        assert rover.getPosition().getCoordinate().getX() == 5;
        assert rover.getPosition().getCoordinate().getY() == 2;

        // When
        rover.forward();
    }
}