package com.htxiong.covata.marsrover;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PlateauTest {
    private Plateau plateau = null;

    @Before
    public void setUp() throws Exception {
        plateau = new Plateau();
    }

    @After
    public void tearDown() throws Exception {
        plateau = null;
    }

    @Test
    public void testToString() throws Exception {
        // Given
        Coordinate upperRight = new Coordinate(5, 5);
        plateau.setUpperRight(upperRight);

        // Then
        assert "bottomLeft is 0 0, and upperRight is 5 5".equals(plateau.toString());

        // Given
        Coordinate bottomLeft = new Coordinate(4, 0);
        plateau.setBottomLeft(bottomLeft);

        // Then
        assert "bottomLeft is 4 0, and upperRight is 5 5".equals(plateau.toString());
    }

    @Test
    public void testIsInPlateau() throws Exception {
        Coordinate upperRight = new Coordinate(5, 5);
        plateau.setUpperRight(upperRight);
        Coordinate bottomLeft = new Coordinate(0, 0);
        plateau.setBottomLeft(bottomLeft);

        // Given
        Coordinate targetCoordinate = new Coordinate(3, 2);

        // Then
        assert plateau.isInPlateau(targetCoordinate) == true;

        // Given
        targetCoordinate = new Coordinate(-1, 2);

        // Then
        assert plateau.isInPlateau(targetCoordinate) == false;

        // Given
        targetCoordinate = new Coordinate(0, 6);

        // Then
        assert plateau.isInPlateau(targetCoordinate) == false;

        // Given
        targetCoordinate = new Coordinate(6, 2);

        // Then
        assert plateau.isInPlateau(targetCoordinate) == false;

        // Given
        targetCoordinate = new Coordinate(0, -1);

        // Then
        assert plateau.isInPlateau(targetCoordinate) == false;

        // Given
        targetCoordinate = new Coordinate(6, 6);

        // Then
        assert plateau.isInPlateau(targetCoordinate) == false;
    }
}